package com.kgc.loginandregister.service;

import com.kgc.loginandregister.dto.UserShopping;
import com.kgc.loginandregister.mapper.UserShoppingMapper;
import com.kgc.loginandregister.utils.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * create by Leo 2020/06/18
 */
@Service
public class RegisterService {

    @Autowired
    private UserShoppingMapper userShoppingMapper;

    public int register(String name, String password, String sex, String information) {
        UserShopping userShopping = new UserShopping();
        userShopping.setId(UUID.randomUUID().toString().substring(0, 10));
        userShopping.setName(name);
        userShopping.setPassword(MD5Utils.md5password(password));
        userShopping.setSex(sex);
        userShopping.setInformation(information);
        return userShoppingMapper.insertSelective(userShopping);
    }
}
