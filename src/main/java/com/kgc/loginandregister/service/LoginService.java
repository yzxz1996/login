package com.kgc.loginandregister.service;


import com.alibaba.fastjson.JSONObject;
import com.kgc.loginandregister.dto.UserShopping;
import com.kgc.loginandregister.dto.UserShoppingExample;
import com.kgc.loginandregister.mapper.UserShoppingMapper;
import com.kgc.loginandregister.utils.MD5Utils;
import com.kgc.loginandregister.utils.RedisUtils;
import com.kgc.loginandregister.vo.UserShoppingVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * create by Leo 2020/06/18
 */
@Service
public class LoginService {
    @Autowired
    private UserShoppingMapper userShoppingMapper;
    @Autowired
    private RedisUtils redisUtils;

    public int checkUser(String name) {
        UserShoppingExample userShoppingExample = new UserShoppingExample();
        userShoppingExample.createCriteria().andNameEqualTo(name);
        long row = userShoppingMapper.countByExample(userShoppingExample);
        if (row > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public String checkPwd(String name, String password) {
        UserShoppingExample userShoppingExample = new UserShoppingExample();
        userShoppingExample.createCriteria().andNameEqualTo(name).andPasswordEqualTo(MD5Utils.md5password(password));
        long row = userShoppingMapper.countByExample(userShoppingExample);
        if (row > 0) {
            UserShoppingVo userShoppingVo = new UserShoppingVo();
            List<UserShopping> userShoppings = userShoppingMapper.selectByExample(userShoppingExample);
            BeanUtils.copyProperties(userShoppings.get(0), userShoppingVo);
            String userStr = JSONObject.toJSONString(userShoppingVo);
            redisUtils.set(userShoppingVo.getId(), userStr);
            return userShoppingVo.getId();
        } else {
            return null;
        }
    }

}
