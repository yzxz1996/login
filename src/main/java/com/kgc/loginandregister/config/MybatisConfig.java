package com.kgc.loginandregister.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * create by Leo 2020/06/16
 */
@Configuration
@MapperScan(basePackages = "com.kgc.loginandregister.mapper")
public class MybatisConfig {
}
