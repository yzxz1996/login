package com.kgc.loginandregister.vo;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

/**
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table usershopping
 *
 * @mbg.generated do_not_delete_during_merge
 */

@Component
public class UserShoppingVo implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column usershopping.id
     *
     * @mbg.generated
     */
    private static final long serialVersionUID = 1448442582123394781L;
    private String id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column usershopping.name
     *
     * @mbg.generated
     */
    private String name;


    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column usershopping.sex
     *
     * @mbg.generated
     */
    private String sex;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column usershopping.information
     *
     * @mbg.generated
     */
    private String information;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column usershopping.create_time
     *
     * @mbg.generated
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column usershopping.update_time
     *
     * @mbg.generated
     */
    private Date updateTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column usershopping.id
     *
     * @return the value of usershopping.id
     * @mbg.generated
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column usershopping.id
     *
     * @param id the value for usershopping.id
     * @mbg.generated
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column usershopping.name
     *
     * @return the value of usershopping.name
     * @mbg.generated
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column usershopping.name
     *
     * @param name the value for usershopping.name
     * @mbg.generated
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column usershopping.sex
     *
     * @return the value of usershopping.sex
     * @mbg.generated
     */
    public String getSex() {
        return sex;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column usershopping.sex
     *
     * @param sex the value for usershopping.sex
     * @mbg.generated
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column usershopping.information
     *
     * @return the value of usershopping.information
     * @mbg.generated
     */
    public String getInformation() {
        return information;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column usershopping.information
     *
     * @param information the value for usershopping.information
     * @mbg.generated
     */
    public void setInformation(String information) {
        this.information = information;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column usershopping.create_time
     *
     * @return the value of usershopping.create_time
     * @mbg.generated
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column usershopping.create_time
     *
     * @param createTime the value for usershopping.create_time
     * @mbg.generated
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


}