package com.kgc.loginandregister.controller;

import com.alibaba.fastjson.JSONObject;
import com.kgc.loginandregister.service.LoginService;
import com.kgc.loginandregister.service.RegisterService;
import com.kgc.loginandregister.utils.RedisUtils;
import com.kgc.loginandregister.utils.ReturnResult;
import com.kgc.loginandregister.utils.ReturnResultUtils;
import com.kgc.loginandregister.vo.UserShoppingVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * create by Leo 2020/06/18
 */

@RestController
@RequestMapping(value = "userShopping")
public class LoginAndRegisterController {
    @Autowired
    private RegisterService registerService;


    @Autowired
    private LoginService loginService;

    @Autowired
    private RedisUtils redisUtils;

    @GetMapping(value = "register")
    public ReturnResult register(@RequestParam String name, @RequestParam String password, @RequestParam String sex, @RequestParam String information) {
        try {
            registerService.register(name, password, sex, information);
            return ReturnResultUtils.returnSuccess(200,"注册成功") ;
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(500,"注册失败") ;
        }
    }

    @GetMapping(value = "login")
    public ReturnResult<UserShoppingVo> login(@RequestParam String name, @RequestParam String password) {

        if (loginService.checkUser(name) == 1) {
            if (loginService.checkPwd(name, password) != null) {
                String jsonStr= redisUtils.get(loginService.checkPwd(name, password)).toString();
                UserShoppingVo userShoppingVo = JSONObject.parseObject(jsonStr,UserShoppingVo.class);
                return ReturnResultUtils.returnSuccess(userShoppingVo,200,"登录成功");
            } else {
                return ReturnResultUtils.returnFail(500, "用户密码错误");
            }
        } else {
            return ReturnResultUtils.returnFail(500, "没有这个用户");
        }
    }
}
