package com.kgc.loginandregister.utils;

import lombok.Data;

/**
 * create by Leo 2020/06/19
 */
@Data
public class ReturnResult<T> {
    private int code;
    private String msg;
    private T data;
}
