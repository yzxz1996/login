package com.kgc.loginandregister.utils;

/**
 * create by Leo 2020/06/19
 */
public class ReturnResultUtils {

    public static ReturnResult returnSuccess(int code, String msg) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMsg(msg);
        return returnResult;
    }

    public static <T> ReturnResult returnSuccess(T data,int code, String msg) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMsg(msg);
        returnResult.setData(data);
        return returnResult;
    }

    public static ReturnResult returnFail(int code, String msg) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMsg(msg);
        return returnResult;
    }
}
